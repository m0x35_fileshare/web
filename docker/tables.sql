create table share_url (url VARCHAR(10) not null, file_id BINARY(16), storage_instance BINARY(16), user_id BINARY(16), primary key (url)) engine=InnoDB;
create table storage_instance (id BINARY(16) not null, download_url varchar(255), name varchar(255), url varchar(255), user_id BINARY(16), primary key (id)) engine=InnoDB;
create table storage_sign_request (request_id BINARY(16) not null, storage_id BINARY(16), primary key (request_id)) engine=InnoDB;
create table user (id BINARY(16) not null, email varchar(255), password varchar(255), username varchar(255), primary key (id)) engine=InnoDB;
alter table user add constraint UK_ob8kqyqqgmefl0aco34akdtpe unique (email);
