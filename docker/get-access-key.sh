#!/bin/bash

CERTS_PATH=$1

mkdir certs

openssl genrsa -out certs/access.key 2048
openssl req -new -key certs/access.key -out certs/access.csr -nodes

openssl x509 -req -CAcreateserial -CAkey $CERTS_PATH/access-ca.key \
    -CA $CERTS_PATH/access-ca.crt -sha256 -days 3650 \
    -in certs/access.csr -out certs/access.crt

openssl pkcs12 -export -out certs/access.p12 \
    -inkey certs/access.key \
    -in certs/access.crt \
    -certfile $CERTS_PATH/access-ca.crt
