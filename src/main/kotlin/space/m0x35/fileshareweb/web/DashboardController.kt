package space.m0x35.fileshareweb.web

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

@Controller
@RequestMapping("/dashboard")
class DashboardController {
    @GetMapping("")
    fun generatePage(): Any {
        val modelview = ModelAndView("dashboard")
        return modelview
    }
}