package space.m0x35.fileshareweb.web

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

@Controller
@RequestMapping("/storage")
class StorageController {
    @GetMapping("/add")
    fun addStoragePage(): Any {
        val modelview = ModelAndView("addstorage")
        modelview.addObject("add", true)
        modelview.addObject("edit", false)

        return modelview
    }

    @GetMapping("/")
    fun editStoragePage(): Any {
        val modelview = ModelAndView("addstorage")
        modelview.addObject("add", false)
        modelview.addObject("edit", true)

        return modelview
    }
}