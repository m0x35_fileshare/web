package space.m0x35.fileshareweb.web

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.view.RedirectView
import space.m0x35.fileshareweb.data.UserData
import space.m0x35.fileshareweb.data.UserRepository
import java.util.regex.Pattern


@Controller
@RequestMapping("/signup")
class SingupController {
    val emailRegex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)

    @Autowired
    lateinit var userRepo: UserRepository

    @GetMapping("")
    fun disaplyForm(): ModelAndView {
        return registerModelView()
    }

    @PostMapping("")
    fun handleCredentials(@RequestParam email: String,
                          @RequestParam username: String,
                          @RequestParam password: String): Any {


        if (!checkEmail(email))
            return registerModelView().addObject("emailError", true)

        val newUser = UserData(id = null,
                username = username,
                password = BCryptPasswordEncoder().encode(password),
                email = email)

        if (userRepo.findByEmail(email).isPresent)
            return registerModelView().addObject("emailAlreadyUsed", true)

        userRepo.save(newUser)

        return redirectToLogin()
    }

    private fun registerModelView() = ModelAndView("signup")
    private fun redirectToLogin() = RedirectView("/login#after-signup")
    private fun checkEmail(email: String) = emailRegex.matcher(email).find()
}