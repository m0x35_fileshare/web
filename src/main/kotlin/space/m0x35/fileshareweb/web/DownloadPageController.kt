package space.m0x35.fileshareweb.web

import org.apache.http.HttpResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView
import space.m0x35.fileshareweb.data.ShareUrlRepository
import space.m0x35.fileshareweb.data.StorageInstanceRepository

@Controller
@RequestMapping("/download")
class DownloadPageController {
    @Autowired
    private lateinit var shareUrlRepo: ShareUrlRepository

    @Autowired
    private lateinit var storageRepository: StorageInstanceRepository

    @GetMapping("/{url}")
    fun generatePage(@PathVariable url: String): Any {

        val oShareUrl = shareUrlRepo.findByUrl(url)
        if (!oShareUrl.isPresent)
            return ResponseEntity.status(HttpStatus.NOT_FOUND)

        val shareUrl = oShareUrl.get()

        val oStorage = storageRepository.findById(shareUrl.storageInstance!!)
        if (!oStorage.isPresent)
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)

        val modelview = ModelAndView("download")
        modelview.addObject("direct_url", "${oStorage.get().downloadUrl}/download/${shareUrl.fileId}")
        modelview.addObject("title", "Download")
        return modelview
    }
}
