package space.m0x35.fileshareweb.data

import org.springframework.data.repository.CrudRepository
import java.util.*

interface ShareUrlRepository : CrudRepository<ShareUrl, String> {
    fun findByUrl(url: String): Optional<ShareUrl>
    fun findByUserId(userId: UUID): Iterable<ShareUrl>
    fun findByStorageInstanceAndFileId(storageInstanceId: UUID, fileId: UUID): Iterable<ShareUrl>
    fun findByStorageInstance(storageInstanceId: UUID): Iterable<ShareUrl>
    fun save(url: ShareUrl): ShareUrl
}