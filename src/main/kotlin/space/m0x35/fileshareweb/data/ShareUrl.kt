package space.m0x35.fileshareweb.data

import java.util.UUID
import javax.persistence.*

@Entity
@EntityListeners(IdGenerator.ShareUrlIdGenerator::class)
@Table(name = "share_url")
data class ShareUrl (
    @Id
    @Column(name = "url", columnDefinition = "VARCHAR(10)", length = 10)
    var url: String? = null,

    @Column(name = "user_id", columnDefinition = "BINARY(16)", length = 16)
    var userId: UUID? = null,

    @Column(name = "storage_instance", columnDefinition = "BINARY(16)", length = 16)
    var storageInstance: UUID? = null,

    @Column(name = "file_id", columnDefinition = "BINARY(16)", length = 16)
    var fileId: UUID? = null
)
