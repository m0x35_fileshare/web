package space.m0x35.fileshareweb.data

import org.springframework.data.repository.CrudRepository
import java.util.Optional

interface UserRepository : CrudRepository<UserData, String> {
    fun findByEmail(email: String): Optional<UserData>
    fun save(user: UserData)
}