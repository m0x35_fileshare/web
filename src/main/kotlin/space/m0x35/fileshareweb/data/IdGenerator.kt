package space.m0x35.fileshareweb.data

import java.util.UUID
import java.util.Random
import javax.persistence.PrePersist

class IdGenerator {

    class FileshareNodeIdGenerator {
        @PrePersist
        fun setId(data: StorageInstance) {
            if (data.id == null)
                data.id = UUID.randomUUID()
        }
    }

    class UserIdGenerator {
        @PrePersist
        fun setId(data: UserData) {
            if (data.id == null)
                data.id = UUID.randomUUID()
        }
    }

    class SignRequestIdGenerator {
        @PrePersist
        fun setId(data: SignRequest) {
            if (data.requestId == null)
                data.requestId = UUID.randomUUID()
        }
    }

    class ShareUrlIdGenerator {
        @PrePersist
        fun setId(data: ShareUrl) {
            if (data.url != null)
                return

            val dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_+"

            val random = Random()
            var link = ""

            for (i in 0 .. 9) {
                val idx = random.nextInt(dictionary.length)
                link += dictionary[idx]
            }

            data.url = link
        }
    }
}