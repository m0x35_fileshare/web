package space.m0x35.fileshareweb.data

import java.util.*
import javax.persistence.*

@Entity
@EntityListeners(IdGenerator.FileshareNodeIdGenerator::class)
@Table(name = "storage_instance")
data class StorageInstance(
        @Id
        @Column(name = "id", columnDefinition = "BINARY(16)", length = 16)
        var id: UUID? = null,
        @Column(name = "user_id", columnDefinition = "BINARY(16)", length = 16)
        var userId: UUID? = null,
        var url: String = "",
        @Column(name = "download_url")
        var downloadUrl: String = "",
        var name: String = ""
)
