package space.m0x35.fileshareweb.data

import org.springframework.data.repository.CrudRepository
import java.util.*

interface StorageInstanceRepository: CrudRepository<StorageInstance, UUID> {
    fun findByUserId(id: UUID): Iterable<StorageInstance>
    fun save(fsnode: StorageInstance): StorageInstance
}