package space.m0x35.fileshareweb.data

import java.util.UUID
import javax.persistence.*

@Entity
@EntityListeners(IdGenerator.SignRequestIdGenerator::class)
@Table(name = "storage_sign_request")
data class SignRequest (
    @Id
    @Column(name = "request_id", columnDefinition = "BINARY(16)", length = 16)
    var requestId: UUID? = null,

    @Column(name = "storage_id", columnDefinition = "BINARY(16)", length = 16)
    var storageId: UUID? = null
)