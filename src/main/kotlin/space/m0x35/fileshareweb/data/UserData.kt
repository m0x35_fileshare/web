package space.m0x35.fileshareweb.data

import java.util.UUID
import javax.persistence.*

@Entity
@EntityListeners(IdGenerator.UserIdGenerator::class)
@Table(name = "user")
data class UserData (
        @Id
        @Column(name = "id", columnDefinition = "BINARY(16)", length = 16)
        var id: UUID? = null,
        var username: String = "",
        var password: String = "",
        @Column(unique = true)
        var email: String = ""
)
