package space.m0x35.fileshareweb.data

import org.springframework.data.repository.CrudRepository
import java.util.*

interface StorageSignRequestRepository : CrudRepository<SignRequest, UUID> {
    fun save(req: SignRequest): SignRequest
    fun findByStorageId(id: UUID): Optional<SignRequest>
}