package space.m0x35.fileshareweb.rest

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class StorageRightsCheckerProvider {
    @Bean
    fun getStorageRightsChecker() = StorageRightsChecker()
}