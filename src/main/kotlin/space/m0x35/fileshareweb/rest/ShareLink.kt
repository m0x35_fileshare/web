package space.m0x35.fileshareweb.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import space.m0x35.fileshareweb.config.ApiPath
import space.m0x35.fileshareweb.data.ShareUrl
import space.m0x35.fileshareweb.data.ShareUrlRepository
import space.m0x35.fileshareweb.data.StorageInstanceRepository
import space.m0x35.fileshareweb.dto.User
import java.net.URL
import java.util.*

@RestController
@RequestMapping(value = [ApiPath.external + "sharelink", ApiPath.web + "sharelink"])
class ShareLink {

    @Autowired
    private lateinit var shareUrlRepo: ShareUrlRepository

    @Autowired
    private lateinit var rightsChecker: StorageRightsChecker

    @Autowired
    private lateinit var storageRepository: StorageInstanceRepository

    data class LinkData(
            var storageInstanceId: UUID? = null,
            var fileId: UUID? = null
    )

    data class DownloadLink(
            val download: String
    )

    @PostMapping
    fun add(@RequestBody linkData: LinkData, authentication: Authentication): Any {
        val user = getUser(authentication)
        val link = ShareUrl(storageInstance = linkData.storageInstanceId,
                            fileId = linkData.fileId,
                            userId = user.getId())

        return shareUrlRepo.save(link)
    }

    @GetMapping
    fun get(@RequestParam storageId: UUID?, @RequestParam fileId: UUID?, authentication: Authentication): Any {
        val user = getUser(authentication)

        if (storageId != null) {
            val optionalStorage = storageRepository.findById(storageId)
            if (!optionalStorage.isPresent)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No such storage instance")
            if (!rightsChecker.checkRights(user.getId(), optionalStorage.get()))
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

            if (fileId != null)
                return shareUrlRepo.findByStorageInstanceAndFileId(storageId, fileId)
            return shareUrlRepo.findByStorageInstance(storageId)
        }

        return shareUrlRepo.findByUserId(user.getId())
    }

    @GetMapping("/{link}/download-url")
    fun getDownloadLink(@PathVariable link: String, authentication: Authentication): Any {
        val user = getUser(authentication)

        val oUrl = shareUrlRepo.findByUrl(link)
        if (!oUrl.isPresent)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Link not found")

        val url = oUrl.get()

        if (url.userId != user.getId())
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("it's not your url!")

        val storage = storageRepository.findById(url.storageInstance!!)
        return DownloadLink("${storage.get().url}/download/${url.fileId}")
    }

    @DeleteMapping("/{link}")
    fun deleteShareUrl(@PathVariable link: String, authentication: Authentication): Any {
        val user = getUser(authentication)

        val oUrl = shareUrlRepo.findByUrl(link)
        if (!oUrl.isPresent)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Link not found")

        val url = oUrl.get()

        if (url.userId != user.getId())
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("it's not your url!")

        shareUrlRepo.delete(url)

        return ResponseEntity.status(HttpStatus.OK).body("")
    }

    private fun getUser(authentication: Authentication) = authentication.principal as User
}