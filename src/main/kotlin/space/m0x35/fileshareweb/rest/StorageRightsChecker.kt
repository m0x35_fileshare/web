package space.m0x35.fileshareweb.rest

import org.springframework.security.core.Authentication
import space.m0x35.fileshareweb.data.StorageInstance
import space.m0x35.fileshareweb.dto.User
import java.util.*

class StorageRightsChecker {
    fun checkRights(userId: UUID, storage: StorageInstance): Boolean {
        return userId == storage.userId
    }

    fun checkRights(authentication: Authentication, storage: StorageInstance): Boolean {
        return checkRights(getUser(authentication).getId(), storage)
    }

    private fun getUser(authentication: Authentication) = authentication.principal as User
}