package space.m0x35.fileshareweb.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import space.m0x35.fileshareweb.config.ApiPath
import space.m0x35.fileshareweb.data.UserRepository

@RestController
@RequestMapping("/checkemail")
class CheckEmail {
    @Autowired
    lateinit var userRepo: UserRepository

    data class EmailReqBody(
            var email: String = ""
    )

    @PostMapping("")
    fun checkEmail(@RequestBody email: EmailReqBody): Any {
        return mapOf(Pair("isFree", !userRepo.findByEmail(email.email).isPresent))
    }
}