package space.m0x35.fileshareweb.rest

import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpGet
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import space.m0x35.fileshareweb.config.ApiPath
import space.m0x35.fileshareweb.data.SignRequest
import space.m0x35.fileshareweb.data.StorageInstance
import space.m0x35.fileshareweb.data.StorageInstanceRepository
import space.m0x35.fileshareweb.data.StorageSignRequestRepository
import space.m0x35.fileshareweb.dto.User
import space.m0x35.fileshareweb.storage.HttpClientManager
import java.util.*

@RestController
@RequestMapping(value = [ApiPath.external + "storage", ApiPath.web + "storage"])
class Storage {

    @Autowired
    private lateinit var storageInstanceRepository: StorageInstanceRepository
    @Autowired
    private lateinit var storageSignRequestRepository: StorageSignRequestRepository
    @Autowired
    private lateinit var httpClientManager: HttpClientManager

    @Autowired
    private lateinit var rightsChecker: StorageRightsChecker

    private fun getUser(authentication: Authentication) = authentication.principal as User

    data class StorageData(
            var name: String = "",
            var url: String = "",
            var downloadUrl: String = ""
    )

    data class SignRequestId(val signRequestCode: UUID?)

    @GetMapping
    fun getAll(authentication: Authentication) : Any {
        return storageInstanceRepository.findByUserId(getUser(authentication).getId())
    }

    @GetMapping("/{instanceId}")
    fun get(@PathVariable instanceId: UUID, authentication: Authentication): Any {
        val optionalStorage = storageInstanceRepository.findById(instanceId)

        if (!optionalStorage.isPresent)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No such storage instance")

        val storage = optionalStorage.get()
        if (!rightsChecker.checkRights(authentication, storage))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

        return storage
    }

    @PostMapping
    fun add(@RequestBody storage: StorageData, authentication: Authentication): Any {
        val user = getUser(authentication)
        val data = StorageInstance(id = null, url = storage.url, downloadUrl = storage.downloadUrl, userId = user.getId(), name = storage.name)
        return ResponseEntity.status(HttpStatus.CREATED).body(storageInstanceRepository.save(data))
    }

    @PutMapping("/{instanceId}")
    fun update(@RequestBody reqStorage: StorageInstance, @PathVariable instanceId: UUID, authentication: Authentication): Any {
        val user = getUser(authentication)

        if (reqStorage.id != instanceId)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Path variable for id and id from the request body differs")

        val optionalStorage = storageInstanceRepository.findById(instanceId)
        if (!optionalStorage.isPresent)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No such storage instance")

        val storage = optionalStorage.get()
        if (!rightsChecker.checkRights(authentication, storage))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

        if (reqStorage.userId != storage.userId)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Changing of user id is not supported")

        storageInstanceRepository.save(reqStorage)

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("")
    }

    @DeleteMapping("/{instanceId}")
    fun delete(@PathVariable instanceId: UUID, authentication: Authentication): Any {
        val user = getUser(authentication)

        val optionalStorage = storageInstanceRepository.findById(instanceId)
        if (!optionalStorage.isPresent)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No such storage instance")

        val storage = optionalStorage.get()
        if (!rightsChecker.checkRights(authentication, storage))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

        storageInstanceRepository.delete(storage)

        return ResponseEntity.status(HttpStatus.OK).body("")
    }

    @GetMapping("/{instanceId}/request-sign-code")
    fun generateSignCode(@PathVariable instanceId: UUID, authentication: Authentication): Any {
        val optionalStorage = storageInstanceRepository.findById(instanceId)

        if (!optionalStorage.isPresent)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No such storage instance")

        val storage = optionalStorage.get()
        if (!rightsChecker.checkRights(authentication, storage))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

        var req = SignRequest(null, storage.id)
        req = storageSignRequestRepository.save(req)
        return ResponseEntity.status(HttpStatus.CREATED).body(SignRequestId(req.requestId!!))
    }

    @GetMapping("/{instanceId}/get-sign-code")
    fun getSignCode(@PathVariable instanceId: UUID, authentication: Authentication): Any {
        val optionalStorage = storageInstanceRepository.findById(instanceId)

        if (!optionalStorage.isPresent)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No such storage instance")

        val storage = optionalStorage.get()
        if (!rightsChecker.checkRights(authentication, storage))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

        val req = storageSignRequestRepository.findByStorageId(instanceId)
        return SignRequestId(if (req.isPresent) req.get().requestId else null)
    }

    @GetMapping("/{instanceId}/status")
    fun checkStatus(@PathVariable instanceId: UUID, authentication: Authentication): Any {
        val optionalStorage = storageInstanceRepository.findById(instanceId)

        if (!optionalStorage.isPresent)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No such storage instance")

        val storage = optionalStorage.get()
        if (!rightsChecker.checkRights(authentication, storage))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

        val client = httpClientManager.getHttpClient()

        val urlOverHttps = storage.url + "/status"
        val getMethod = HttpGet(urlOverHttps)

        val response = client.execute(getMethod)
        val responseBody = response.entity.content.reader().readText()
        val responseCode = response.statusLine.statusCode
        return ResponseEntity.status(responseCode).body(responseBody)
    }
}
