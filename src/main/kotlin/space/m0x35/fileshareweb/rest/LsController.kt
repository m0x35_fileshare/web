package space.m0x35.fileshareweb.rest

import org.apache.http.client.methods.HttpGet
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import space.m0x35.fileshareweb.config.ApiPath
import space.m0x35.fileshareweb.data.StorageInstanceRepository
import space.m0x35.fileshareweb.storage.HttpClientManager
import java.util.*

@RestController
@RequestMapping(value = [ApiPath.external + "ls", ApiPath.web + "ls"])
class LsController {

    @Autowired
    private lateinit var httpClientManager: HttpClientManager

    @Autowired
    private lateinit var storageInstanceRepository: StorageInstanceRepository

    @Autowired
    private lateinit var rightsChecker: StorageRightsChecker

    @GetMapping("/{storageIdStr}")
    fun listRoot(@PathVariable storageIdStr: String, authentication: Authentication): Any {
        val storageId = UUID.fromString(storageIdStr)
        val optionalStorage = getStorage(storageId)

        if (!optionalStorage.isPresent)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No such storage instance")

        val storage = optionalStorage.get()

        if (!rightsChecker.checkRights(authentication, storage))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

        val client = httpClientManager.getHttpClient()

        val urlOverHttps = storage.url + "/ls"
        val getMethod = HttpGet(urlOverHttps)

        val response = client.execute(getMethod)
        val responseBody = response.entity.content.reader().readText()
        val responseCode = response.statusLine.statusCode
        return ResponseEntity.status(responseCode).body(responseBody)
    }

    @GetMapping("/{storageIdStr}/{dirIdStr}")
    fun listDir(@PathVariable storageIdStr: String, @PathVariable dirIdStr: String, authentication: Authentication): Any {
        val storageId = UUID.fromString(storageIdStr)
        val optionalStorage = getStorage(storageId)

        if (!optionalStorage.isPresent)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No such storage instance")

        val storage = optionalStorage.get()

        if (!rightsChecker.checkRights(authentication, storage))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You don't have rights to use this storage")

        val client = httpClientManager.getHttpClient()

        val urlOverHttps = storage.url + "/ls/" + dirIdStr
        val getMethod = HttpGet(urlOverHttps)

        val response = client.execute(getMethod)
        val responseBody = response.entity.content.reader().readText()
        val responseCode = response.statusLine.statusCode
        return ResponseEntity.status(responseCode).body(responseBody)
    }

    private fun getStorage(storageId: UUID) = storageInstanceRepository.findById(storageId)
}