package space.m0x35.fileshareweb.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter
import org.springframework.security.web.savedrequest.RequestCacheAwareFilter


class SecurityConfig {

    @Configuration
    public class ApiSecurity : WebSecurityConfigurerAdapter() {

        @Bean
        @Throws(Exception::class)
        override fun authenticationManagerBean(): AuthenticationManager {
            return super.authenticationManagerBean()
        }
    }

    @Configuration
    @EnableWebSecurity(debug = true)
    @EnableAutoConfiguration
    @Order(1)
    public class WebApiSecurity : WebSecurityConfigurerAdapter() {
        @Throws(Exception::class)
        override fun configure(http: HttpSecurity) {
            http
                    .formLogin()
                    .permitAll()
                    .and()
                    .authorizeRequests()
                    .antMatchers("/signup", "/", "/checkemail", "/download/*")
                    .permitAll()
                    .and()
                    .authorizeRequests()
                    .anyRequest()
                    .authenticated()
                    .antMatchers(ApiPath.web + "**")
                    .authenticated()
        }
    }
}
