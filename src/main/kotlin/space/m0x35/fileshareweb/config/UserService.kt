package space.m0x35.fileshareweb.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import space.m0x35.fileshareweb.data.UserRepository
import space.m0x35.fileshareweb.data.UserData
import space.m0x35.fileshareweb.dto.User

@Service("userDetailsService")
class UserService : UserDetailsService {

    @Autowired
    private lateinit var userRepository: UserRepository

    override fun loadUserByUsername(username: String): UserDetails {

        val optionalUser = userRepository.findByEmail(username)
        if (!optionalUser.isPresent)
            throw UsernameNotFoundException("No such user $username")

        return userFromData(optionalUser.get())
    }

    fun userFromData(data: UserData) = User(data.id!!, data.email, data.password)

}
