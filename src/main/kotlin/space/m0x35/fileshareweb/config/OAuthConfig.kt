package space.m0x35.fileshareweb.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore


@Configuration
class OAuthConfig {

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun tokenStore() : TokenStore {
        return InMemoryTokenStore();
    }

    @Configuration
    @EnableAuthorizationServer
    class AuthConfig : AuthorizationServerConfigurerAdapter() {
        @Autowired
        @Qualifier("userDetailsService")
        private lateinit var userDetailsService: UserDetailsService

        @Autowired
        private lateinit var authenticationManager: AuthenticationManager

        @Autowired
        private lateinit var tokenStorage: TokenStore

        @Throws(Exception::class)
        override fun configure(configurer: AuthorizationServerEndpointsConfigurer) {
            configurer.authenticationManager(authenticationManager)
            configurer.userDetailsService(userDetailsService)
            configurer.tokenStore(tokenStorage)
        }

        @Throws(Exception::class)
        override fun configure(clients: ClientDetailsServiceConfigurer) {
            clients.inMemory()
                    .withClient("cli")
                    .secret(BCryptPasswordEncoder().encode("secret"))
                    .scopes("read")
                    .authorizedGrantTypes("password", "refresh_token", "client_credentials")
        }
    }

    @EnableResourceServer
    class ReourseServerConfig: ResourceServerConfigurerAdapter() {

        @Autowired
        private lateinit var tokenStorage: TokenStore

        override fun configure(resources: ResourceServerSecurityConfigurer) {
            resources.tokenStore(tokenStorage)
        }

        override fun configure(http: HttpSecurity) {
            http
                    .antMatcher(ApiPath.external + "**")
                    .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                    .exceptionHandling()
                    .accessDeniedHandler(OAuth2AccessDeniedHandler())
        }

    }
}