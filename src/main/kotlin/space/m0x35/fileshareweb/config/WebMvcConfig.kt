package space.m0x35.fileshareweb.config

import com.fasterxml.jackson.databind.PropertyNamingStrategy
import org.springframework.web.servlet.view.InternalResourceViewResolver
import org.springframework.web.servlet.ViewResolver
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.*
import com.fasterxml.jackson.databind.MapperFeature
import org.springframework.boot.convert.ApplicationConversionService.configure
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.converter.HttpMessageConverter


@Configuration
@EnableWebMvc
class WebMvcConfig : WebMvcConfigurer {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    override fun configureDefaultServletHandling(configurer: DefaultServletHandlerConfigurer) {
        configurer.enable()
    }

    override fun extendMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        super.extendMessageConverters(converters)
        objectMapper.propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
        for (httpConverter in converters) {
            if (httpConverter is MappingJackson2HttpMessageConverter) {
                // register the configured object mapper to HttpMessageconter
                httpConverter.objectMapper = objectMapper;
            }
        }
    }

    @Bean
    fun viewResolver(): ViewResolver {
        val bean = InternalResourceViewResolver()
        bean.setPrefix("/WEB-INF/")
        bean.setSuffix(".jsp")
        return bean
    }
}