package space.m0x35.fileshareweb.config;

public class ApiPath {
    public static final String external = "/api/";
    public static final String web = "/web-api/";
}
