package space.m0x35.fileshareweb.storage

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Config {
    @Bean
    fun httpClientManager() = HttpClientManager()
}