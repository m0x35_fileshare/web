package space.m0x35.fileshareweb.storage

import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import java.io.FileInputStream
import java.security.KeyStore
import org.apache.http.client.methods.HttpGet
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.ssl.SSLContexts
import org.apache.http.ssl.TrustStrategy
import org.springframework.beans.factory.annotation.Value
import java.security.cert.X509Certificate

class HttpClientManager {

    @Value("\${storageclients.ssl.keypath}")
    private lateinit var keyPath : String

    fun getHttpClient(): CloseableHttpClient {
        val passwd = ""
        val keyInStream = FileInputStream(keyPath)
        val keyStore = KeyStore.getInstance("PKCS12")
        keyStore.load(keyInStream, passwd.toCharArray())

        val trustStrategy = object : TrustStrategy {
            override fun isTrusted(chain: Array<out X509Certificate>?, authType: String?) = true
        }

        val sslContext = SSLContexts
                .custom()
                .loadKeyMaterial(keyStore, passwd.toCharArray())
                .loadTrustMaterial(trustStrategy)
                .build()

        val sslsf = SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE)

        return HttpClients
                .custom()
                .setSSLSocketFactory(sslsf)
                .build()

    }
}