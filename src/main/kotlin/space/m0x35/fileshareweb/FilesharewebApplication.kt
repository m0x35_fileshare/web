package space.m0x35.fileshareweb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FilesharewebApplication

fun main(args: Array<String>) {
	runApplication<FilesharewebApplication>(*args)
}
