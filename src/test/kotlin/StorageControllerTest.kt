import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import space.m0x35.fileshareweb.data.StorageInstanceRepository
import java.util.*
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import space.m0x35.fileshareweb.FilesharewebApplication
import space.m0x35.fileshareweb.data.SignRequest
import space.m0x35.fileshareweb.data.StorageInstance
import space.m0x35.fileshareweb.data.StorageSignRequestRepository
import space.m0x35.fileshareweb.dto.User


@RunWith(SpringRunner::class)
@SpringBootTest(classes = [ FilesharewebApplication::class ])
@AutoConfigureMockMvc(secure = false)
class StorageControllerTest {
    @Autowired
    private lateinit var storageRepo: StorageInstanceRepository

    @Autowired
    private lateinit var storageSignReqRepo: StorageSignRequestRepository

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    private fun makeUser() = User(UUID.randomUUID(), "", "")

    private fun saveStorage(user: User): StorageInstance {
        val testUrl = "https://localhost:3765"
        val testDownloadUrl = "https://localhost"
        val name = "testname"
        val storage = StorageInstance(userId = user.getId(), downloadUrl = testDownloadUrl, url = testUrl, name = name)
        return storageRepo.save(storage)
    }

    private fun saveSignRequest(storage: StorageInstance): SignRequest {
        val signRequest = SignRequest(storageId = storage.id)
        return storageSignReqRepo.save(signRequest)
    }

    @Test
    fun getById() {
        val user = makeUser()
        val storage = saveStorage(user)

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/storage/${storage.id}")
                                .with(SecurityMockMvcRequestPostProcessors.user(user))
                )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().json("""{id: ${storage.id},
                        user_id: ${user.getId()},
                        url: "${storage.url}",
                        download_url: "${storage.downloadUrl}",
                        name: "${storage.name}"}"""))
                .andDo(MockMvcResultHandlers.print())
    }

    @Test
    fun signCodeShouldBeNull() {
        val user =  makeUser()
        val storage = saveStorage(user)

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/storage/${storage.id}/get-sign-code")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().json("{ sign_request_code: null }"))
                .andDo(MockMvcResultHandlers.print())
    }

    @Test
    fun signCodeShouldBeValid() {
        val user = makeUser()
        val storage = saveStorage(user)
        val signRequest = saveSignRequest(storage)

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/storage/${storage.id}/get-sign-code")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().json("{ sign_request_code: ${signRequest.requestId} }"))
                .andDo(MockMvcResultHandlers.print())
    }

    @Test
    fun createSignCode() {
        val user = makeUser()
        val storage = saveStorage(user)

        val result = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/storage/${storage.id}/request-sign-code")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
        )
                .andExpect(MockMvcResultMatchers.status().isCreated)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        val rawRepsonse = result.response.contentAsString

        //read JSON like DOM Parser
        val response = objectMapper.readTree(rawRepsonse)
        val signReqNode = response.path("sign_request_code")
        Assert.assertTrue(signReqNode != null)
        val reqId = signReqNode.asText()

        val signReq = storageSignReqRepo.findByStorageId(storage.id!!)
        Assert.assertTrue(signReq.isPresent)
        Assert.assertEquals(signReq.get().requestId, UUID.fromString(reqId))
    }

    @Test
    fun createStorage() {
        val user = makeUser()
        val testUrl = "https://localhost:4589"
        val testDownloadUrl = "https://localhost:4570"
        val testName = "Test name"

        val result = mockMvc.perform(
                MockMvcRequestBuilders.post("/api/storage/")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"url\": \"$testUrl\", \"name\": \"$testName\", \"download_url\": \"$testDownloadUrl\" }")
        )
                .andExpect(MockMvcResultMatchers.status().isCreated)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        val rawRepsonse = result.response.contentAsString

        //read JSON like DOM Parser
        val response = objectMapper.readTree(rawRepsonse)

        val id = response.path("id")
        Assert.assertTrue(id.isTextual)

        val storage = storageRepo.findById(UUID.fromString(id.asText())).get()

        val name = response.path("name")
        Assert.assertEquals(testName, name.asText())
        Assert.assertEquals(testName, storage.name)

        val url = response.path("url")
        Assert.assertEquals(testUrl, url.asText())
        Assert.assertEquals(testUrl, storage.url)

        val downloadUrl = response.path("download_url")
        Assert.assertEquals(testDownloadUrl, downloadUrl.asText())
        Assert.assertEquals(testDownloadUrl, storage.downloadUrl)

        val userId = response.path("user_id")
        Assert.assertEquals(user.getId().toString(), userId.asText())
        Assert.assertEquals(user.getId(), storage.userId)
    }

    @Test
    fun updateStorage() {
        val user = makeUser()
        val storage = saveStorage(user)

        storage.name = "new name"
        storage.url = "https://new-url"

        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/storage/${storage.id}")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(storage))
        )
                .andExpect(MockMvcResultMatchers.status().isNoContent)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        val resultingStorage = storageRepo.findById(storage.id!!).get()
        Assert.assertEquals(storage.url, resultingStorage.url)
        Assert.assertEquals(storage.name, resultingStorage.name)
    }

    @Test
    fun deleteStorage() {
        val user = makeUser()
        val storage = saveStorage(user)

        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/storage/${storage.id}")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        Assert.assertFalse(storageRepo.findById(storage.id!!).isPresent)
    }
}
