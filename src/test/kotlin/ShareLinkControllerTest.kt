import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpSession
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import space.m0x35.fileshareweb.FilesharewebApplication
import space.m0x35.fileshareweb.data.*
import space.m0x35.fileshareweb.dto.User


@RunWith(SpringRunner::class)
@SpringBootTest(classes = [ FilesharewebApplication::class ])
@AutoConfigureMockMvc(secure = true)
class ShareLinkControllerTest {
    @Autowired
    private lateinit var storageRepo: StorageInstanceRepository

    @Autowired
    private lateinit var shareUrlRepository: ShareUrlRepository

    @Autowired
    private lateinit var storageSignReqRepo: StorageSignRequestRepository

    @Autowired
    private lateinit var mockMvc: MockMvc

    private fun makeUser() = User(UUID.randomUUID(), "", "")

    private fun saveStorage(user: User): StorageInstance {
        val testUrl = "http://localhost"
        val name = "testname"
        val storage = StorageInstance(userId = user.getId(), url = testUrl, name = name)
        return storageRepo.save(storage)
    }

    private fun saveShareLink(user: User, storage: StorageInstance): ShareUrl {
        val url = ShareUrl(userId = user.getId(), storageInstance = storage.id, fileId = UUID.randomUUID())
        return shareUrlRepository.save(url)
    }

    @Test
    fun createShareUrl() {
        val user = makeUser()
        val storage = saveStorage(user)
        val fileId = UUID.randomUUID()

        val result = mockMvc.perform(
                MockMvcRequestBuilders.post("/web-api/sharelink")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"storage_instance_id\": \"${storage.id}\", \"file_id\": \"$fileId\" }")
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        val rawRepsonse = result.response.contentAsString
        val objectMapper = ObjectMapper()

        //read JSON
        val response = objectMapper.readTree(rawRepsonse)
        val url = response.path("url")
        Assert.assertTrue(url.isTextual)
        Assert.assertFalse(url.textValue().isEmpty())

        //check repo
        val urlFormRepo = shareUrlRepository.findByUrl(url.textValue())
        Assert.assertTrue(urlFormRepo.isPresent)
    }

    @Test
    fun getShareUrl() {
        val user = makeUser()
        val storage = saveStorage(user)
        val link = saveShareLink(user, storage)

        val result = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/sharelink")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        val rawRepsonse = result.response.contentAsString
        val objectMapper = ObjectMapper()

        //read JSON
        val response = objectMapper.readTree(rawRepsonse)
        val node = response.elements().next()
        val url = node.path("url")
        Assert.assertEquals(link.url, url.textValue())

        val storageId = node.path("storage_instance")
        Assert.assertEquals(storage.id, UUID.fromString(storageId.textValue()))

        val fileId = node.path("file_id")
        Assert.assertEquals(link.fileId, UUID.fromString(fileId.textValue()))
    }

    @Test
    fun getShareUrlByStorageAndFileId() {
        val user = makeUser()
        val storage = saveStorage(user)
        val link = saveShareLink(user, storage)
        saveShareLink(user, storage)
        saveShareLink(user, storage)

        val getReqUrl = "/api/sharelink?storageId=${link.storageInstance}&fileId=${link.fileId}"
        val result = mockMvc.perform(
                MockMvcRequestBuilders.get(getReqUrl)
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        val rawRepsonse = result.response.contentAsString
        val objectMapper = ObjectMapper()

        //read JSON
        val response = objectMapper.readTree(rawRepsonse)
        val elements = response.elements()
        val node = elements.next()
        Assert.assertFalse(elements.hasNext())

        val url = node.path("url")
        Assert.assertEquals(link.url, url.textValue())

        val storageId = node.path("storage_instance")
        Assert.assertEquals(storage.id, UUID.fromString(storageId.textValue()))

        val fileId = node.path("file_id")
        Assert.assertEquals(link.fileId, UUID.fromString(fileId.textValue()))
    }

    @Test
    fun notEnoughRightsToGetUrl() {
        val user = makeUser()
        val storageUser = makeUser()
        val storage = saveStorage(storageUser)
        saveShareLink(user, storage)

        val result = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/sharelink?storageId=${storage.id}")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
        )
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        val rawRepsonse = result.response.contentAsString
        Assert.assertFalse(rawRepsonse.contains("url:"))
    }

    @Test
    fun getDownloadUrl() {
        val user = makeUser()
        val storageUser = makeUser()
        val storage = saveStorage(storageUser)
        val link = saveShareLink(user, storage)

        val result = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/sharelink/${link.url}/download-url")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()

        val rawRepsonse = result.response.contentAsString
        val objectMapper = ObjectMapper()

        //read JSON
        val response = objectMapper.readTree(rawRepsonse)
        val downloadUrl = response.path("download")
        Assert.assertTrue(downloadUrl.isTextual)
        Assert.assertFalse(downloadUrl.textValue().isEmpty())
    }

    @Test
    fun deleteShareUrl() {
        val user = makeUser()
        val storageUser = makeUser()
        val storage = saveStorage(storageUser)
        val link = saveShareLink(user, storage)

        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/sharelink/${link.url}")
                        .with(SecurityMockMvcRequestPostProcessors.user(user))
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(MockMvcResultHandlers.print())
                .andReturn()
    }
}